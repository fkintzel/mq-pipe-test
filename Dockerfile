# This file is a template, and might need editing before it works on your project.
FROM python:3.6

WORKDIR /usr/src/app
COPY . /usr/src/app

#CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# For some other command
# CMD ["python", "app.py"]
