#!/usr/bin/env python
import time, os, traceback


import logging
logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
try:
    os.mkfifo("/tmp/pipes/elg-in")
    os.mkfifo("/tmp/pipese/lg-out")
except:
    traceback.print_exc()
requestQ = open("/tmp/pipes/elg-in", "w", 1)
responseQ = open("/tmp/pipes/elg-out", "r", 1)

def on_request():
    text_to_process = "Test on!\n"

    requestQ.write(text_to_process)
    resp=responseQ.readline()
   

    logger.debug("Responding with " + resp)


while (True):
    on_request()
    logger.debug ("sleep")
    time.sleep(1)

