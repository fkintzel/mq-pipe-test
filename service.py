#!/usr/bin/env python
import time, traceback, os


import logging
logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

try:
    os.mkfifo("/tmp/pipes/elg-in")
    os.mkfifo("/tmp/pipes/elg-out")
except:
    traceback.print_exc()
requestQ = open("/tmp/pipes/elg-in", "r", 1)
responseQ = open("/tmp/pipes/elg-out", "w", 1)

while (True):
    
    req = requestQ.readline()
    logger.debug ("received " + str(req))
    responseQ.write("Echo back " + str(req)+"\n")
    
